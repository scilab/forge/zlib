/* ==================================================================== */
/* Allan CORNET - 2011 */
/* ==================================================================== */
#include <stdio.h>
#include <windows.h>
/* ==================================================================== */
#ifdef _WIN64
#pragma comment(lib,"../../thirdparty/zlib/windows/win64/lib/zlib64.lib")
#else
#pragma comment(lib,"../../thirdparty/zlib/windows/win32/lib/zlib32.lib")
#endif
/* ==================================================================== */
int WINAPI DllMain (HINSTANCE hInstance , DWORD reason, PVOID pvReserved)
{
  switch (reason)
  {
  case DLL_PROCESS_ATTACH:
    break;
  case DLL_PROCESS_DETACH:
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return 1;
}
/* ==================================================================== */

