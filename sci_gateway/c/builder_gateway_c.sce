// Allan CORNET - 2011

function builder_gw_c()

  includes_src_c = ilib_include_flag(get_absolute_file_path("builder_gateway_c.sce") + "../../src/c");

  // PutLhsVar managed by user in sci_sum and in sci_sub
  // if you do not this variable, PutLhsVar is added
  // in gateway generated (default mode in scilab 4.x and 5.x)
  WITHOUT_AUTO_PUTLHSVAR = %t;

  functions_list = ["zlib_compressfile", "sci_zlib_compressfile"; ..
                    "zlib_decompressfile", "sci_zlib_decompressfile"; ..
					"zlib_compress", "sci_zlib_compress"; ..
					"zlib_decompress", "sci_zlib_decompress"];

  files_list = ["sci_zlib_compressfile.c", ..
                "sci_zlib_decompressfile.c", ..
				"sci_zlib_compress.c", ..
				"sci_zlib_decompress.c"];

  tbx_build_gateway("gw_zlib_c", ..
                    functions_list, ..
                    files_list, ..
                    get_absolute_file_path("builder_gateway_c.sce"), ..
                    ["../../src/c/libc_zlib"], ..
                    "", ..
                   includes_src_c);
                   
endfunction

builder_gw_c();
clear builder_gw_c; // remove builder_gw_c on stack
